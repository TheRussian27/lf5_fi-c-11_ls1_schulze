import java.util.Scanner;

public class Fahrkartenautomatmitmethoden {
    static Scanner input = new Scanner(System.in);
    static double eingezahlterGesamtbetrag;
    
    public static double fahrkartenbestellungErfassen(double kostenProTicket, short anzahlTickets) {
        return kostenProTicket * anzahlTickets;
    }
    
    public static void fahrkartenbestellungBezahlen(double zuZahlen) {
        eingezahlterGesamtbetrag = 0.0;
        
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            double eingeworfeneM�nze;
            double e = zuZahlen - eingezahlterGesamtbetrag;
            System.out.printf("Noch zu zahlen: " + "%.2f", e);System.out.println(" Euro");
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfeneM�nze = input.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    }
    
    public static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public static void fahrkartenbestellungAusgeben(short anzahlTickets) {
           if(anzahlTickets > 1.9) 
        	   System.out.println("\nFahrscheine werden ausgegeben");
           else 
        	   System.out.println("\nFahrschein wird ausgegeben");
           
           for (int i = 0; i < 10; i++) {
              System.out.print("=");
              warte(100);
           }
           System.out.println("\n\n");
    }
   
    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + "�");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
           double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
           if(r�ckgabebetrag > 0.0)
           {
               System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " Euro");
               System.out.println("wird in folgenden M�nzen ausgezahlt:");
               
               while(r�ckgabebetrag >=30) {
            	   System.out.println("30 Euro Schein");
            	   r�ckgabebetrag -= 30.0;
               }

               while(r�ckgabebetrag >= 2.0) {
            	   muenzeAusgeben(2, "Euro");
            	   r�ckgabebetrag -= 2;
               }
               
               while(r�ckgabebetrag >= 1.0) {
            	   muenzeAusgeben(1, "Euro");
            	   r�ckgabebetrag -= 1;
               }
               
               while(r�ckgabebetrag >= 0.5) {
            	   muenzeAusgeben(50, "Cent");
            	   r�ckgabebetrag -= 0.5;
               }
               
               while(r�ckgabebetrag >= 0.2) {
            	   muenzeAusgeben(20, "Cent");
            	   r�ckgabebetrag -= 0.2;
               }
               
               while(r�ckgabebetrag >= 0.1) {
            	   muenzeAusgeben(10, "Cent");
            	   r�ckgabebetrag -= 0.1;
               }
               
               while(r�ckgabebetrag >= 0.05) {
            	   muenzeAusgeben(5, "Cent");
            	   r�ckgabebetrag -= 0.05;
               }
           }

           System.out.println("\nVergessen Sie nicht den Fahrschein\n"+
                              "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir w�nschen Ihnen eine gute Fahrt.");
        }
    
    public static void main(String[] args) {
        double kostenProTicket;
        short anzahlTickets;
        
        System.out.print("Zu zahlender Betrag (Euro): ");
        kostenProTicket = input.nextDouble();
        System.out.print("Anzahl der Fahrkarten: ");
        anzahlTickets = input.nextShort();
        double zuZahlen = fahrkartenbestellungErfassen(kostenProTicket, anzahlTickets);
        fahrkartenbestellungBezahlen(zuZahlen);
        fahrkartenbestellungAusgeben(anzahlTickets);
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlen);
        
    }
}