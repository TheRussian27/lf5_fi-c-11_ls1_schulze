public class Dezimalzahlen {
	public static void main(String[] args) {
		double z1,z2,z3,z4, z5;
		z1 = 22.4234234;
		z2 = 111.222;
		z3 = 4.0;
		z4 = 1000000.551;
		z5 = 97.34;
		// Ausgabe der 2 Nachkommastellen einer Festkommazahl
		System.out.printf("%.2f\n",z1);
		System.out.printf("%.2f\n",z2);
		System.out.printf("%.2f\n",z3);
		System.out.printf("%.2f\n",z4);
		System.out.printf("%.2f\n",z5);

	}
}