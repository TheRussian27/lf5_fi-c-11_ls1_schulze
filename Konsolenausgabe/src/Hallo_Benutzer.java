import java.util.Scanner;

public class Hallo_Benutzer {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		String name, alter;
		
		System.out.println("Sch�nen guten Tag Benutzer,");
		System.out.println("wie lautet Ihr Name?\n");
		
		name = eingabe.next();
		
		System.out.println("\nund wie alt sind Sie?\n");
		
		alter = eingabe.next();
		
		System.out.println("\nSie hei�en also " + name + " und sind " + alter + " Jahre alt.");
		
		eingabe.close();
	}
}