public class Temperaturtabelle {
	public static void main(String[] args) {
		String s = "|";
		String w = "-----------------------";
		
		//�berschrift
		System.out.printf("%-12s", "Fahrenheit"); System.out.print(s);
		System.out.printf("%10s\n", "Celsius");
		System.out.println(w);
		
		//erster Eintrag
		System.out.printf("%-12s", "-20"); System.out.print(s); // Fahrenheit
		System.out.printf("%10.6s", "-28.8889");				// Celsius
		
		//zweiter Eintrag
		System.out.printf("\n%-12.5s", "-10"); System.out.print	(s);
		System.out.printf("%10.6s", "-23.3333");
		
		//dritter Eintrag
		System.out.printf("\n%-12.5s", "0"); System.out.print(s);
		System.out.printf("%10.6s", "-17.7778");
		
		//vierter Eintrag
		System.out.printf("\n%-12.5s", "20"); System.out.print(s);
		System.out.printf("%10.5s", "-6.6667");
		
		//f�nfter Eintrag
		System.out.printf("\n%-12.5s", "30"); System.out.print(s);
		System.out.printf("%10.5s", "-1.1111");
	}
}