import java.util.Scanner; // Import der Klasse Scanner

public class Rechner {

 public static void main(String[] args) { // Hier startet das Programm


	 // Neues Scanner-Objekt myScanner wird erstellt
	 Scanner myScanner = new Scanner(System.in);

	 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

	 
	 // Die Variable zahl1 speichert die erste Eingabe
	 int zahl1 = myScanner.nextInt();

	 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

	 // Die Variable zahl2 speichert die zweite Eingabe
	 int zahl2 = myScanner.nextInt();
 
 	System.out.printf("%-10s %-1s \n", "Addition", "1");
 	System.out.printf("%-10s %-1s \n", "Subtraktion", "2");
 	System.out.printf("%-10s %-1s \n", "Division", "3");
 	System.out.printf("%-10s %-1s \n \n", "Multiplikation", "4");
 	System.out.print("W�hlen sie die Grundoperation: ");

 	int rechenart, ergebnis;
 	rechenart = myScanner.nextInt();
 	ergebnis = 0;
 	
 	if (rechenart == 1)
 	{
 		ergebnis = zahl1 + zahl2;
 		System.out.print("\n Das Ergebnis der Addition aus " + zahl1 + " und " + zahl2 + " ergibt " + ergebnis);
 	}	
 		else if (rechenart == 2)
 		{
 			ergebnis = zahl1 - zahl2;
 			System.out.print("\n Das Ergebnis der Subtraktion aus " + zahl1 + " und " + zahl2 + " ergibt " + ergebnis);
 		}
 			else if (rechenart == 3)
 			{
 				ergebnis = zahl1 / zahl2;
 				System.out.print("\n Das Ergebnis der Division aus " + zahl1 + " und " + zahl2 + " ergibt " + ergebnis);
 			}
 				else if (rechenart == 4)
 				{
 					ergebnis = zahl1 * zahl2;
 					System.out.print("\n Das Ergebnis der Multiplikation aus " + zahl1 + " und " + zahl2 + " ergibt " + ergebnis);
 				}
 					else
 					{
 						System.out.println("Fehler, geben Sie eine der gew�hlten Grundoperatoren ein!");
 					}
 	myScanner.close();
 		/*
 		// Die Addition der Variablen zahl1 und zahl2
 		// wird der Variable ergebnis zugewiesen.
 		int ergebnis = zahl1 + zahl2;

 		System.out.print("\n\n\nErgebnis der Addition lautet: ");
 		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
 		myScanner.close();*/

 }
}