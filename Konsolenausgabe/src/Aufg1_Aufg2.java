public class Aufg1_Aufg2 {
	public static void main(String[] args) {
		String s = "**********";
		
		System.out.printf("%6.2s\n", s);
		System.out.printf("%.1s", s);
		System.out.printf("%9.1s\n", s);
		System.out.printf("%.1s", s);
		System.out.printf("%9.1s\n", s);
		System.out.printf("%6.2s\n", s);
		
		//Fakult�t Ausgabe ohne Schleife
		String b = "= ";
		String faku = "1 * 2 * 3 * 4 * 5";
		System.out.printf("\n%-5s", "0!"); System.out.print(b);
		System.out.printf("%19s", ""); System.out.print(b);
		System.out.printf("%4.1s", "1");
		
		System.out.printf("\n%-5s", "1!"); System.out.print(b);
		System.out.printf("%-19.1s", faku); System.out.print(b);
		System.out.printf("%4.1s", "1");
		
		System.out.printf("\n%-5s", "2!"); System.out.print(b);
		System.out.printf("%-19.5s", faku); System.out.print(b);
		System.out.printf("%4.1s", "2");
		
		System.out.printf("\n%-5s", "3!"); System.out.print(b);
		System.out.printf("%-19.9s", faku); System.out.print(b);
		System.out.printf("%4.1s", "6");
		
		System.out.printf("\n%-5s", "4!"); System.out.print(b);
		System.out.printf("%-19.13s", faku); System.out.print(b);
		System.out.printf("%4.2s", "24");
		
		System.out.printf("\n%-5s", "5!"); System.out.print(b);
		System.out.printf("%-19.17s", faku); System.out.print(b);
		System.out.printf("%4.3s", "120");
		
	}
}