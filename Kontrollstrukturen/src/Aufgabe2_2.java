import java.util.Scanner;

public class Aufgabe2_2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int monat;
		String ans;
		
		System.out.println("Geben Sie einen Monat (als Zahl ein) um dessen Bezeichnung zu behalten!");
		monat = input.nextInt();		
		switch(monat) {
			case 1:
				ans = "Januar";
				break;
			case 2:
				ans = "Februar";
				break;
			case 3:
				ans = "M�rz";
				break;
			case 4:
				ans = "April";
				break;
			case 5:
				ans = "Mai";
				break;
			case 6:
				ans = "Juni";
				break;
			case 7:
				ans = "Juli";
				break;
			case 8:
				ans = "August";
				break;
			case 9:
				ans = "September";
				break;
			case 10:
				ans = "Oktober";
				break;
			case 11:
				ans = "November";
				break;
			case 12:
				ans = "Dezember";
				break;
			default:
				ans = "Fehler, Geben Sie eine Zahl im Bereich 1-12 ein";
		}
		System.out.println(ans);
		input.close();
		
	}
}