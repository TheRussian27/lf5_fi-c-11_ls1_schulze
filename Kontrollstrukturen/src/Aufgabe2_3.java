import java.util.Scanner;

public class Aufgabe2_3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		char eingabe;
		String ans;
		
		System.out.println("Geben Sie ein r�misches Zeichen ein, um deren Wert zu erhalten.");
		eingabe = input.next().charAt(0);
		
		switch(eingabe) {
			case 'I':
				ans = "1";
				break;
			case 'V':
				ans = "5";
				break;
			case 'X':
				ans = "10";
				break;
			case 'L':
				ans = "50";
				break;
			case 'C':
				ans = "100";
				break;
			case 'D':
				ans = "500";
				break;
			case 'M':
				ans = "1000";
				break;
			default:
				ans = "Fehler, Geben Sie eine r�mische Zahl ein";
		}
		System.out.println(ans);
		input.close();
	}
}