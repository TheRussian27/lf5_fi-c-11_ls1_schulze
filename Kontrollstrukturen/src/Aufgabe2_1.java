import java.util.Scanner;

class Aufgabe2_1 {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		int note;
		String antwort;
		
		System.out.println("Geben Sie eine Note ein, um den Ausdruck f�r diesen zu erhalten.");
		
		note = eingabe.nextInt();
		
		switch(note) {
			case 1:
				antwort = "Sehr gut";
				break;
			case 2:
				antwort = "Gut";
				break;
			case 3:
				antwort = "Befriedigend";
				break;
			case 4:
				antwort = "Ausreichend";
				break;
			case 5:
				antwort = "Mangelhaft";
				break;
			case 6:
				antwort = "Ungen�gend";
				break;
			default:
				antwort = "Fehler, Geben Sie eine Zahl im Bereich 1 - 6 ein.";
		}
		
		System.out.println("Sie erhalten nun nach der Antwort, eine Meldung.");
		System.out.println(antwort);
		eingabe.close();
	}
}